package jp.alhinc.okada_masaki.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CalculateSales {
	public static void main(String[] args) {

		BufferedReader br = null;
		try {
			File file = new File(args[0],"branch.lst");
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			Map<String,String> map = new HashMap<String,String>();
			Map<String, Long> totalMap = new HashMap<String, Long>();

			//支店定義ファイルの読み込みとデータ保持
			while((line = br.readLine()) != null) {
				String[] lineKeep = line.split(",",0);

				//支店コードが数字のみ、かつ3桁か否か
				Pattern p = Pattern.compile("^[0-9]{3}$");
				Matcher m = p.matcher(lineKeep[0]);
				if(!m.find()) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				//支店名にカンマ、および改行が含まれているか否か
				if(lineKeep.length != 2) {
					System.out.println("支店定義ファイルのフォーマットが不正です。");
					return;
				}

				map.put(lineKeep[0],lineKeep[1]);
				totalMap.put(lineKeep[0],0L);
			}

			//拡張子rcd、ファイル名8桁のフィルタ作成
			File dir = new File(args[0]);

			FilenameFilter filter = new FilenameFilter () {
				public boolean accept(File filename, String str) {
					return str.matches("^[0-9]{8}.rcd$");
				}
			};
			File[] files = dir.listFiles(filter);
			List<String> keepRow = new ArrayList<String>();

			//売上ファイルの読み込み及び、売上金額の加算
			int startNum = Integer.parseInt(files[0].getName().substring(0, 8));
			for(int i = 0; i < files.length; i++) {
				FileReader frb = new FileReader(files[i]);
				br = new BufferedReader(frb);

				//ファイル名の連番エラー処理
				String fileName = files[i].getName();
				int serial = Integer.parseInt(fileName.substring(0, 8));

				if(i != 0 && serial != startNum + i) {
					System.out.println("売上ファイル名が連番になっていません");
					frb.close();
					return;
				}

				String row;
				long ammount = 0;
				while((row = br.readLine()) != null) {
					keepRow.add(row);
				}
				if(totalMap.containsKey(keepRow.get(0))){
					ammount = totalMap.get(keepRow.get(0)) + Long.parseLong(keepRow.get(1));
					totalMap.put(keepRow.get(0), ammount);
					if(ammount >= 1000000000) {
						System.out.println("合計金額が10桁を超えました");
						frb.close();
						return;
					}
				}else {
					System.out.println(fileName + "の支店コードが不正です");
					frb.close();
					return;
				}

				if(keepRow.size() >= 3) {
					System.out.println(fileName + "のフォーマットが不正です");
					frb.close();
					return;
				}

				keepRow.clear();
			}

			//支店別集計ファイルの作成
			File resultFile = new File(args[0],"branch.out");
			FileWriter write = new FileWriter(resultFile);
			PrintWriter pw = new PrintWriter(new BufferedWriter(write));

			for(Map.Entry<String, String> entry : map.entrySet()) {
				pw.println(entry.getKey() + "," + entry.getValue() + "," + totalMap.get(entry.getKey()));
			}
			pw.close();

			FileReader fReader = new FileReader(resultFile);
			br = new BufferedReader(fReader);

			String result;
			while((result = br.readLine()) != null) {
				System.out.println(result);
			}
		}catch(FileNotFoundException fnfe) {
			System.out.println("支店定義ファイルが存在しません");
			return;
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
	}

}
